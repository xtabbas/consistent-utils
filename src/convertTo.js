import * as utils from "../index";
const days = {
  dd: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
  DD: [
    "Sunday",
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday",
  ],
};
const months = {
  mm: [
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "May",
    "Jun",
    "Jul",
    "Aug",
    "Sep",
    "Oct",
    "Nov",
    "Dec",
  ],
  MM: [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December",
  ]
};

/**
 *
 * when converting to minute to determine the position on the schedule
 * we aren't accounting for the timezone offset...
 * which works in our favour because we always want to consider the timezone changes... aka use the current timezone
 *
 * but now in case of timezone agnostic events, which technically will use the timezone the event was created in
 * we need to account the timezone offset, and always "subtract or add" the timezone to bring it back to the original
 *
 * also, it will always work, because events are always saved in "UTC" and the conversion happens in real time
 *
 * so now, technically if we subtract the current active timezone, it will work like how its working now
 * but, if we subtract the event timezone, it may aswell work like we expect it to work!
 *
 */
const convertTo = (time, timezone = "Europe/London") => {
  // const changeTime = utils.changeTimezone(time, timezone);
  // const changedNow = utils.changeTimezone(new Date(), timezone);
  const changeTime = new Date(time);
  const changedNow = new Date();

  // const difference = changeTime.getDate() - changedNow.getDate();

  let cleanStart = utils.onlyDate(changeTime);
  let cleanEnd = utils.onlyDate(changedNow);

  let m = utils.calcTotalMins(cleanEnd, cleanStart);
  let h = m / 60;
  let difference = h / 24;

  const isInPast = difference < 0;
  const isInFuture = difference > 0;

  const totalHours = changeTime.getHours();
  const totalMinutes = changeTime.getMinutes();

  // total mins is hours + mins...
  let totalMins = totalHours * 60 + totalMinutes;

  // we are adding difference in minutes converted to hours
  // 3 days * 24 hours * 60 mintues, but why? because,
  // totalMins doesn't accounts for what day it is, just uses time

  let exactMins = totalMins;
  let extactHours = exactMins / 60;

  if (isInFuture) {
    // now if its in future we need to add 24 hours
    exactMins = totalMins + 60 * (24 * difference);
    extactHours = exactMins / 60;
  }

  if (isInPast) {
    // if its in past we might want to remove 24 hours too
    // bc if we dont, and then it will calc position using toMins()
    exactMins = totalMins - 60 * (24 * difference);
    extactHours = exactMins / 60;
  }

  return {
    // toMins gives total minutes accounting only for the time (not date)
    toMins: () => totalMins,
    // toMinsExact gives total minutes accounting for the time and date (if in past -24 else +24)
    // toMinsExact: () => {
    //   let cleanStart = utils.onlyDate(time);
    //   let cleanEnd = utils.onlyDate(new Date());

    //   let totalMins = utils.calcTotalMins(cleanEnd, cleanStart);

    //   return totalMins;
    // },
    toHours: () => totalMins / 60,
    toHoursExact: () => extactHours,
    toDay: (format) => days[format][changeTime.getDay()],
    toMonth: (format) => months[format][changeTime.getMonth()],
    toDateFromHours: (date = changedNow) => {
      let hours = Math.floor(time);
      let mins = (time - hours) * 60;

      return new Date(new Date(date).setHours(hours, mins, 0, 0)).toISOString();
    },
    toDateFromHoursUTC: (date = changedNow) => {
      let hours = Math.floor(time);
      let mins = (time - hours) * 60;

      var date = new Date(new Date(date).setHours(hours, mins, 0, 0));
      return date.toISOString();
    },

    /**
     * @bug
     * if for some reason, 1 - 30 is passed as start/end
     * 1-30 is 29 and it gets fucked up, where as, it should still pass as 1
     *
     * for this, we can use absolute time, then subtract both times, and convert to hours
     * if hours is less than 24 but greater than 0 its today and we can return 0
     * if hours is greater than 24 its future
     * if hours is less than 24 its past
     *
     * and by dividing by 24 we could get exact difference
     */
    toDifference: (end) => {
      /**
       * basically if its today should return 0
       * if its future should return 1
       * if its past should return -1
       */

      let cleanStart = utils.onlyDate(time);
      let cleanEnd = utils.onlyDate(end);

      let totalMins = utils.calcTotalMins(cleanEnd, cleanStart);
      let totalHours = totalMins / 60;
      let diff = totalHours / 24;

      if (diff > 0) {
        return 1;
      } else if (diff < 0) {
        return -1;
      } else {
        return 0;
      }
    },
    /**
     * @note to difference abs expects clean dates
     */
    toDifferenceAbs: (end) => {
      let totalMins = utils.calcTotalMins(end, time);
      let totalHours = totalMins / 60;
      let diff = totalHours / 24;

      return diff;
    },
  };
};

export default convertTo;
