export const minsStepperRounded = (steps, time) => {
  const clean = new Date(time);
  const hours = clean.getHours();
  const mins = clean.getMinutes();

  clean.setHours(hours, mins + steps, 0, 0);
  return new Date(clean).toISOString();
};

const minsStepper = (steps, time) => {
  const timeString = new Date(time).getTime();
  const plusStepMins = timeString + steps * 60 * 1000;

  return new Date(plusStepMins).toISOString();
};

export default minsStepper;
