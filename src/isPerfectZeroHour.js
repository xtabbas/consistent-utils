const isPerfectZeroHour = (date = new Date()) => {
  const clean = new Date(date);
  const hours = clean.getHours();
  const minutes = clean.getMinutes();
  const seconds = clean.getSeconds();

  if (hours === 0 && minutes === 0 && seconds === 0) {
    return true;
  } else {
    return false;
  }
};

module.exports = isPerfectZeroHour;
