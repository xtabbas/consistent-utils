import * as utils from "../index";

// const addDatesBefore = (array, length) => {
//   if (!length && !Array.isArray(array)) {
//     throw new Error();
//   }

//   const temp = [];
//   for (let i = 1; i < length; i++) {
//     const today = new Date(array[0]);
//     temp.push(new Date(today.setDate(today.getDate() - i)));
//   }

//   return temp.reverse().concat(array);
// };

const addDatesBefore = (array, length) => {
  if (!length && !Array.isArray(array)) {
    throw new Error();
  }

  const temp = [];
  for (let i = 1; i < length; i++) {
    let x = 0 - i;
    temp.push(utils.dayStepper(x, array[0]));
  }

  return temp.reverse().concat(array);
};

export default addDatesBefore;
