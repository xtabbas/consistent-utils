const calcTotalMins = (start, end) => {
  const timeStart = new Date(start).getTime();
  const timeEnd = new Date(end).getTime();

  const totalMins = (timeEnd - timeStart) / (60 * 1000);

  return totalMins;
};

export default calcTotalMins;
