const dayStepper = (steps, date) => {
  
  const timeString = new Date(date).getTime();
  const plusStepHours = timeString + steps * 24 * 60 * 60 * 1000;

  /**
   * Daylight saving bug report::
   *
   * When daylight saving happens in the UK... the day stepper doesn't really sets time to zero hours
   * WTF...
   *
   * So basically before DST zero is zero but after DST zero is -1!!!
   *
   * So basically day stepper was always bugged, where it
   *
   */

  let clean = new Date(plusStepHours);

  // clean.setHours(10, 59, 59, 59);

  if (!clean.toISOString) {
  }
  // console.log("clean ", date, clean);

  return clean.toISOString();
};

export default dayStepper;
