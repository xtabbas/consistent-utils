const onlyDate = (date = new Date()) => {
  const clean = new Date(date);
  const hours = clean.getHours();
  const minutes = clean.getMinutes();
  const seconds = clean.getSeconds();

  // if (hours === 0 && minutes === 0 && seconds === 0) {
  //   // clean = clean;
  // } else if (hours === 0) {
  //   clean.setHours(0, 0, 0, 0);
  // } else {
  // }
  clean.setHours(10, 59, 59, 59);

  if (new Date(date).getDate() !== clean.getDate()) {
    console.error("was is not the same as is for date :", date);
    console.warn(hours, minutes, seconds);
  }

  return clean.toDateString();
};

export default onlyDate;
