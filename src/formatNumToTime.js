import * as utils from "consistent-utils";

export const isWorkHour = (number, expanded, data, offset) => {
  let num = number + offset;
  num = num > 23 ? num - 24 : num < 0 ? num + 24 : num;
  let ampm = num >= 12 ? "pm" : "am";

  num = num % 12;
  num = num ? num : 12; // the hour '0' should be '12'
  let strTime = num + " " + ampm;

  switch (true) {
    case num !== 12 && num >= 8 && ampm === "am":
      return true;

    case num === 12 && ampm === "pm":
      return true;

    case num <= 6 && ampm === "pm":
      return true;

    default:
      return false;
  }
};

export const isFreeTimeActive = (data, number, expanded) => {
  if (data.length < 1) {
    return false;
  }

  if (expanded) {
    return false;
  }

  let firstItem = data[0];
  let lastItem = data[Math.max(data.length - 1, 0)];

  let lastItemIndex = lastItem ? lastItem.stack.index : undefined;

  let isEndTomo =
    utils.convert(new Date(lastItem.end_time)).toDifference(new Date()) > 0;
  let isStartYest =
    utils.convert(new Date(firstItem.start_time)).toDifference(new Date()) < 0;

  if (isEndTomo || isStartYest) {
    return false;
  }

  let now = new Date().getTime();

  if (number === 0) {
    if (new Date(firstItem.start_time).getTime() > now) {
      return true;
    } else {
      return false;
    }
  }

  if (number === lastItemIndex + 1) {
    if (new Date(lastItem.end_time).getTime() < now) {
      return true;
    } else {
      return false;
    }
  }

  let [finalET, finalST] = findFinalTimes(data, number);

  switch (true) {
    case finalET === finalST:
      return false;
    case finalET >= finalST:
      return false;
    default:
      break;
  }

  let start = new Date(finalST).getTime();
  let end = new Date(finalET).getTime();

  if (now > end && now < start) {
    return true;
  } else {
    return false;
  }
};

const findFinalTimes = (data, number) => {
  let ii = data.find((event) => event.stack.index === number);
  let pp = data.find((event) => event.stack.index === number - 1);

  let prevEventWithGreaterEndTime = data.find((event) => {
    if (
      event.stack.index < number &&
      (event.end_time > pp.end_time || event.end_time > ii.end_time)
    ) {
      return true;
    }
  });

  let finalET = pp.end_time;
  let finalST = ii.start_time;

  let pdt = prevEventWithGreaterEndTime
    ? prevEventWithGreaterEndTime.end_time
    : undefined;

  if (pdt) {
    finalET = prevEventWithGreaterEndTime.end_time;
  }

  return [finalET, finalST];
};

const formatNumToTime = (number, expanded, data, offset, tz) => {
  if (expanded) {
    // clocks (or offset) can go forward or backwards... cater for backwards...

    let sum = number + offset;
    let num = number + offset;
    num = num > 23 ? num - 24 : num < 0 ? num + 24 : num;
    let ampm = num >= 12 ? "pm" : "am";
    num = num % 12;
    num = num ? num : 12; // the hour '0' should be '12'
    num = `${num}`;
    num = num.split(".");
    num = num[1] ? `${num[0]} ${num[1] * 6}` : num[0];

    let string =
      num +
      " " +
      ampm +
      " " +
      `${sum > 24 ? "(tomo)" : sum < 0 ? "(yest)" : ""}`;
    return { number, string };
  } else {
    // num is 1, 2 ... n where n is data.length
    // to select first item in data... do number - 1

    let prevEndTime;
    let nextStartTime;

    // if we have data ofcourse
    if (data.length > 0) {
      let firstItem = data[0];
      let lastItem = data[Math.max(data.length - 1, 0)];
      let lastItemIndex = lastItem ? lastItem.stack.index : undefined;

      // so if we can say last item is data.length - 1...
      // then, we can say
      // second item is

      // if its the first item...
      if (number === 0) {
        return {
          number: new Date(firstItem.start_time).getHours() - 1,
          string:
            "free time - all day before " +
            utils.formatAMPM(firstItem.start_time, tz),
        };
      }

      // if its the last item... number is is 8 and
      if (number === lastItemIndex + 1) {
        return {
          number: new Date(lastItem.end_time).getHours() + 2,
          string:
            "free time - all day after " +
            utils.formatAMPM(lastItem.end_time, tz),
        };
      }

      // save the event end time for one event
      // and if the next event end time is less than that then use saved one

      // pick event where number equals stack index...
      let [finalET, finalST] = findFinalTimes(data, number);

      prevEndTime = utils.hoursStepper(0.02, finalET);
      nextStartTime = utils.hoursStepper(-0.01, finalST);

      switch (true) {
        case finalET === finalST:
          return {
            number: new Date(finalST).getHours(),
            string: "no free time - back to back events",
          };
        case finalET >= finalST:
          return {
            number: new Date(finalET).getHours(),
            string: "no free time - conflicting events",
          };
        default:
          break;
      }

      // if second event end time is greater... update the saved time
    }

    return {
      number: new Date(prevEndTime).getHours() + 1,
      string:
        "free time - " +
        utils.formatAMPM(prevEndTime, tz) +
        " to " +
        utils.formatAMPM(nextStartTime, tz),
    };
  }
};

export default formatNumToTime;
