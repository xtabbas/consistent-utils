const hoursStepper = (steps, time) => {
  const timeString = new Date(time).getTime();
  const plusStepHours = timeString + steps * 60 * 60 * 1000;

  return new Date(plusStepHours).toISOString();
};

export default hoursStepper;
