import calcTotalMins from "./calcTotalMins";

/**
 * Basically, inversed will filter out all day events,
 * and, non inversed will filter out everything but all day events
 */

export default (data) => {
  return (
    data?.filter((e) => {
      let totalMins = calcTotalMins(e.start_time, e.end_time);
      let totalHours = totalMins / 60;

      if (totalHours >= 23.9) {
        return true;
      } else {
        return false;
      }
    }) || []
  );
};
