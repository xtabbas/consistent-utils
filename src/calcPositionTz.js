import * as utils from "../index";

function calcPosition(item, hourBlockHeight) {
  const heightOfMin = hourBlockHeight / 60;
  let zIndex = 100;
  let left = 0;

  let {
    start_time: startTime,
    end_time: endTime,
    date: today = new Date(),
  } = item;

  // to find event height (if expanded) min height * num of mins
  const totalActTime = utils.calcTotalMins(startTime, endTime);
  let itemHeight = heightOfMin * totalActTime;

  // if starting minute is in yesterday, it returns in negative (-)
  let startingMinute = utils.convert(startTime).toMins();
  // let startingMinuteExact = utils.convert(startTime).toMinsExact();

  let endingHour = utils.convert(endTime).toHours();

  let startingPosition;

  // now to find where this event must be placed, aka
  // the y offset of this event, aka, how much it should move from top
  // from starting time in mins, convert into hrs, times hr height
  // + 52 bc container padding, works in our favour, padding is added on timeline > a
  // pure-container doesn't have padding, items are always laid based on top offset

  let isEndTomo = utils.convert(endTime).toDifference(today) > 0;
  let isStartYest = utils.convert(startTime).toDifference(today) < 0;

  if (isEndTomo) {
    // act ending is tomor > reduce height
    startingPosition = (startingMinute / 60) * hourBlockHeight;
    // cap the height - subtract hours that are in tomorrow
    let heightInTomo = endingHour * hourBlockHeight;
    itemHeight = itemHeight - heightInTomo;
  } else if (isStartYest) {
    // act is starting yesterday
    startingPosition = 0 - (24 - startingMinute / 60) * hourBlockHeight;
    // reduce height!
  } else {
    // act is today
    startingPosition = (startingMinute / 60) * hourBlockHeight;
  }

  return {
    event: {
      ...item,
      // yesterday: Math.sign(startingMinuteExact) < 0,
    },
    length: totalActTime,
    height: itemHeight,
    top: startingPosition + hourBlockHeight,
    startingMinute,
    // zIndex,
    // left,
    // transform: "rotate3d(25, -127, 0, 40deg)",
    // transform: left > 0 ? "rotate3d(25, -127, 0, 40deg)" : "none",
    // transform: left > 0 ? "rotate3d(15, -107, 0, 35deg)" : "none",
    // transform: "rotate3d(15, -155, 0, 50deg)",
    // transform: "rotate3d(15, -155, 0, 71deg)",
  };
}

export default calcPosition;
