import moment from "moment-timezone";
import * as utils from "../index";

const formatAMPM = (d, tz) => {
  let date = new Date(d);

  let date2 = moment.utc(date).tz(tz).format();

  let date3 = new Date(date2).toISOString();

  var changed = utils.changeTimezone(date, tz);

  date = changed;

  var hours = date.getHours();
  var minutes = date.getMinutes();
  return ("0" + hours).slice(-2) + ":" + ("0" + minutes).slice(-2) + " " + moment().tz(tz).format("z");
  var ampm = hours >= 12 ? "pm" : "am";
  hours = hours % 12;
  hours = hours ? hours : 12;
  minutes = minutes < 10 ? "0" + minutes : minutes;
  var strTime = hours + ":" + minutes + " " + ampm;
  return strTime;
};

export const formatTime = (d, tz) => {
  let date = new Date(d);

  let date2 = moment.utc(date).tz(tz).format();

  let date3 = new Date(date2).toISOString();

  var changed = utils.changeTimezone(date, tz);

  date = changed;

  var hours = date.getHours();
  var minutes = date.getMinutes();
  return ("0" + hours).slice(-2) + ":" + ("0" + minutes).slice(-2) + " " + moment().tz(tz).format("z");

  hours = hours % 12;
  hours = hours ? hours : 12;
  minutes = minutes < 10 ? "0" + minutes : minutes;
  var strTime = hours + ":" + minutes;
  return strTime;
};

export const onlyAMPM = (d, tz) => {
  let date = new Date(d);

  let date2 = moment.utc(date).tz(tz).format();

  let date3 = new Date(date2).toISOString();

  var changed = utils.changeTimezone(date, tz);

  date = changed;

  var hours = date.getHours();
  var ampm = hours >= 12 ? "pm" : "am";
  return ampm;
};

export default formatAMPM;
