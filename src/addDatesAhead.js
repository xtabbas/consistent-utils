import * as utils from "../index";
// const addDatesAhead = (array, length) => {
//   if (!length && !Array.isArray(array)) {
//     throw new Error();
//   }
//   const temp = [...array];
//   for (let i = 1; i < length; i++) {
//     const today = new Date(array[array.length - 1]);
//     temp.push(new Date(today.setDate(today.getDate() + i)));
//   }

//   return temp;
// };

const addDatesAhead = (array, length) => {
  if (!length && !Array.isArray(array)) {
    throw new Error();
  }
  const temp = [...array];
  for (let i = 1; i < length; i++) {
    temp.push(utils.dayStepper(i, array[array.length - 1]));
  }

  return temp;
};

export default addDatesAhead;
