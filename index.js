import convert from "./src/convertTo";
import addDatesAhead from "./src/addDatesAhead";
import addDatesBefore from "./src/addDatesBefore";
import calcTotalMins from "./src/calcTotalMins";
import formatAMPM, { formatTime, onlyAMPM } from "./src/formatAMPM";
import formatNumToTime, {
  isWorkHour,
  isFreeTimeActive,
} from "./src/formatNumToTime";
import hoursStepper from "./src/hoursStepper";
import onlyDate from "./src/onlyDate";
import calcPositionTz from "./src/calcPositionTz";
// import calcConflictOffset from "./src/calcConflictOffset";
import dayStepper from "./src/dayStepper";
import changeTimezone from "./src/changeTimezone";
import filterAlldayEvents from "./src/filterAlldayEvents";
import filterNonAlldayEvents from "./src/filterNonAlldayEvents";
import isPerfectZeroHour from "./src/isPerfectZeroHour";
import minsStepper, { minsStepperRounded } from "./src/minsStepper";

let findWeekNumber = (date) => {
  if (date <= 7) {
    return 1;
  } else if (date > 7 && date <= 14) {
    return 2;
  } else if (date > 14 && date <= 21) {
    return 3;
  } else if (date > 21) {
    return 4;
  }
};

let convertStringToUTC = (string) => {
  let time = string;
  let date = string.substring(0, 8);
  let year = date.substring(0, 4);
  let month = date.substring(4, 6);
  let day = date.substring(6, 8);

  time = time.split("T")[1];

  if (time) {
    let hour = time.substring(0, 2);
    let minute = time.substring(2, 4);
    let second = time.substring(4, 6);

    // JS MONTH STARTS WITH ZEROOOO
    return Date.UTC(year, month - 1, day, hour, minute, second);
  } else {
    return Date.UTC(year, month - 1, day);
  }
};

export {
  addDatesAhead,
  addDatesBefore,
  calcTotalMins,
  convert,
  formatAMPM,
  formatTime,
  onlyAMPM,
  formatNumToTime,
  hoursStepper,
  onlyDate,
  calcPositionTz,
  // calcConflictOffset,
  dayStepper,
  minsStepper,
  minsStepperRounded,
  isWorkHour,
  changeTimezone,
  isFreeTimeActive,
  filterAlldayEvents,
  filterNonAlldayEvents,
  isPerfectZeroHour,
  findWeekNumber,
  convertStringToUTC,
};
